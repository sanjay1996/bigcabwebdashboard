import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import PropTypes from "prop-types";
import _ from "lodash";
import { FormattedMessage } from "react-intl";
import "../../styles/common/mobileapp_configuration.scss";

class tripPriceCalculation extends Component {

  state = {
    rideType: 'Economy',
    currentType: {
      currencySymbol: "$",
      baseFare: 0,
      farePerKm: 0,
      farePerMin: 0
    },
    rideObj: null,
  }

  static propTypes = {
    tripObj: PropTypes.object,
    onUpdateTrip: PropTypes.func
  };

  componentWillMount = () => {
    this.setState({
      rideObj: this.props.tripObj,
      currentType: this.props.tripObj.tripPrice,
    });
  }


  handleChange(value, label, updateReq) {
    if (this.state.rideType === 'Economy') {
      if (label !== "currencySymbol") {
        this.props.tripObj.tripPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.tripPrice[label] = value;
      }
      this.props.onUpdateTrip(
        this.props.tripObj.tripPrice,
        "tripPrice",
        updateReq
      );
    } else if (this.state.rideType === 'Premium') {
      if (label !== "currencySymbol") {
        this.props.tripObj.tripPrice1[label] = parseFloat(value);
      } else {
        this.props.tripObj.tripPrice1[label] = value;
      }
      this.props.onUpdateTrip(
        this.props.tripObj.tripPrice1,
        "tripPrice1",
        updateReq
      );
    } else if (this.state.rideType === 'SUV') {
      if (label !== "currencySymbol") {
        this.props.tripObj.tripPrice2[label] = parseFloat(value);
      } else {
        this.props.tripObj.tripPrice2[label] = value;
      }
      this.props.onUpdateTrip(
        this.props.tripObj.tripPrice2,
        "tripPrice2",
        updateReq
      );
    } else if (this.state.rideType === 'Luxury') {
      if (label !== "currencySymbol") {
        this.props.tripObj.tripPrice3[label] = parseFloat(value);
      } else {
        this.props.tripObj.tripPrice3[label] = value;
      }
      this.props.onUpdateTrip(
        this.props.tripObj.tripPrice3,
        "tripPrice3",
        updateReq
      );
    }

  }

  clearFormData() {

  }

  render() {
    return (
      <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div className="panel panel-primary Panelprice">
          <div className="panel-heading">
            <span>
              {" "}
              <FormattedMessage
                id={"trip_price"}
                defaultMessage={"Trip Price Calculation"}
              />
            </span>
          </div>
          <div className="panel-body panelBody" style={{ padding : 0}}>
            <div className="priceCalculation">
              <div className={this.state.rideType === 'Economy' ? "carTypeActive" : "carType"} onClick={() => this.setState({ currentType: this.props.tripObj.tripPrice, rideType: 'Economy' })}>Economy</div>
              <div className={this.state.rideType === 'Premium' ? "carTypeActive" : "carType"} onClick={() => this.setState({ currentType: this.props.tripObj.tripPrice1, rideType: 'Premium' })}>Premium</div>
              <div className={this.state.rideType === 'SUV' ? "carTypeActive" : "carType"} onClick={() => this.setState({ currentType: this.props.tripObj.tripPrice2, rideType: 'SUV' })}>SUV</div>
              <div className={this.state.rideType === 'Luxury' ? "carTypeActive" : "carType"} onClick={() => this.setState({ currentType: this.props.tripObj.tripPrice3, rideType: 'Luxury' })}>Luxury</div>
            </div>
            <Form inline className="col-md-12 col-lg-12 col-sm-12 form">
              <FormGroup
                controlId="formInlineName"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4 ">
                  <FormattedMessage
                    id={"currency_symbol"}
                    defaultMessage={"Currency Symbol"}
                  />
                </ControlLabel>

                <FormControl
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  type="text"
                  placeholder={_.get(this.state.currentType, "currencySymbol", "$")}
                  onBlur={event => {
                    this.handleChange(
                      event.target.value,
                      "currencySymbol",
                      true
                    ); event.target.value = ""
                  }
                  }
                />
              </FormGroup>
              <FormGroup
                controlId="formInlineEmail"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  {" "}
                  <FormattedMessage
                    id={"base_fare"}
                    defaultMessage={"Base Fare:"}
                  />
                </ControlLabel>
                <FormControl
                  type="email"
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "baseFare", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "baseFare", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>

              <FormGroup
                controlId="formInlineName"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  <FormattedMessage
                    id={"fare_km"}
                    defaultMessage={"Fare Per Km:"}
                  />
                </ControlLabel>

                <FormControl
                  type="text"
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "farePerKm", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "farePerKm", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>

              <FormGroup
                controlId="formInlineEmail"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  <FormattedMessage
                    id={"fare_min"}
                    defaultMessage={"Fare Per Min:"}
                  />
                </ControlLabel>

                <FormControl
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "farePerMin", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "farePerMin", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

export default tripPriceCalculation;
