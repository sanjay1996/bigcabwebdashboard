import {
    FETCH_USER_QUERIES,
    FETCH_USER_QUERIES_FAILED,
    FETCH_USER_QUERIES_SUCCESS,
    UPDATE_USER_QUERIES_STATUS,
    UPDATE_USER_QUERIES_STATUS_SUCCESS,
    UPDATE_USER_QUERIES_STATUS_FAILED

} from './actionType';

import QueryAction from '../../services/queries';

function fetchingUserQueries(payload) {
    return {
        type: FETCH_USER_QUERIES,
    };
}

function fetchingUserQueriesSuccess(payload) {
    return {
        type: FETCH_USER_QUERIES_SUCCESS,
        payload
    };
}

function fetchingUserQueriesFailed(payload) {
    return {
        type: FETCH_USER_QUERIES_FAILED,
        payload
    };
}

function updatingUserQueries() {
    return {
        type: UPDATE_USER_QUERIES_STATUS,
    };
}

function updateUserQueriesSuccess(payload) {
    return {
        type: UPDATE_USER_QUERIES_STATUS_SUCCESS,
        payload
    };
}

function updateUserQueriesFailed(payload) {
    return {
        type: UPDATE_USER_QUERIES_STATUS_FAILED,
        payload
    };
}


function fetchUserQueries(payload) {
    return (dispatch, getState) => {
        const token = getState().auth.user.jwtAccessToken;
        dispatch(fetchingUserQueries());
        QueryAction.getAllUserQueries(token)
            .then(response => {
                if (response.success) {
                    dispatch(fetchingUserQueriesSuccess(response));
                }
                else {
                    dispatch(fetchingUserQueriesFailed(response));
                }
            })
            .catch(e => {
                if (e.message === "Unauthorized") {
                    dispatch({ type: "FLUSH_DATA" });
                }
                dispatch(fetchingUserQueriesFailed(e));
            });
    };
}

function updateUserQueries(payload) {
    return (dispatch, getState) => {
        const token = getState().auth.user.jwtAccessToken;
        dispatch(updatingUserQueries(payload));
        QueryAction.updateQueryStatus(token, payload)
            .then(response => {
                if (response.success) {
                    dispatch(updateUserQueriesSuccess(response));
                }
                else {
                    dispatch(updateUserQueriesFailed(response));
                }
            })
            .catch(e => {
                if (e.message === "Unauthorized") {
                    dispatch({ type: "FLUSH_DATA" });
                }
                dispatch(updateUserQueriesFailed(e));
            });
    };
}

export default {
    fetchUserQueries,
    updateUserQueries
}