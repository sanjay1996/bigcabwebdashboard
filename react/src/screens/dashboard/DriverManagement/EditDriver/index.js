import React, { Component } from "react";
import Layout from "../../../../components/Layout/layout";
import EditProfile from "./EditDriverProfile";

export default class EditDriverProfile extends Component {
    render() {
        return (
            <Layout>
                <EditProfile />
            </Layout>
        );
    }
}