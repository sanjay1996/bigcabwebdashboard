import React, { Component } from "react";
import Layout from "../../../components/Layout/layout";
import Queries from "./Queries";

export default class UserQueries extends Component {
    render() {
        return (
            <Layout>
                <Queries />
            </Layout>
        );
    }
}
