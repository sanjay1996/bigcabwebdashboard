import React, { Component } from "react";
import { connect } from "react-redux";
import {
    Pagination,
    Button,
    FormControl,
    DropdownButton,
    MenuItem,
    InputGroup
} from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import moment from "moment";
import _ from "lodash";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import UserAction from "../../../redux/users/action";
import "../../../styles/common/ridesList.scss";
import RideDetails from "../../../components/RideHistoryDetails";
class RideHistory extends Component {


    constructor(props) {
        super(props);
        this.state = {
            searchTerm: "",
            ridesList: null,
            searchType: "MOBILE",
            openDetails: false,
            selectedRide: null,
        }
    }
    componentWillMount() {
        this.props.fetchRideHistory();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.ridesHistory !== null && nextProps.ridesHistory !== undefined) {
            this.setState({
                ...this.state,
                ridesList: nextProps.ridesHistory.data
            })
        }
    }

    filteredProducts(searchTerm) {
        const ridesList = this.state.ridesList;
        let list = null;

        if (ridesList !== undefined && searchTerm !== '' && searchTerm.length > 1) {
            if (this.state.searchType === "E-MAIL") {
                list = ridesList.filter((ride) => ride.riderId.email.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1);
            }
            else if (this.state.searchType === "MOBILE") {
                list = ridesList.filter((ride) => ride.riderId.phoneNo.indexOf(searchTerm) !== -1);
            }

            this.state.ridesList = list;
        } else if (searchTerm === '' || searchTerm.length <= 1) {
            this.state.ridesList = this.props.ridesHistory.data;
        }
    }


    render() {
        return (
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="panel panel-primary">
                    <div className="panel-heading panelheading">
                        <span>

                            {" "}
                            <FormattedMessage
                                id={"ride_history"}
                                defaultMessage={"RIDE HISTORY"}
                            />
                        </span>
                        {this.state.openDetails === false ?
                            <div className="search-box">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder={`SEARCH RIDE BY ${this.state.searchType}`}
                                    value={this.state.searchTerm}
                                    onChange={(event) => {
                                        this.setState({ searchTerm: event.target.value });
                                        this.filteredProducts(this.state.searchTerm);
                                    }}

                                />
                                <select className="form-control" onChange={(event) => this.setState({ searchType: event.target.value })}>
                                    <option >MOBILE</option>
                                    <option >E-MAIL</option>
                                </select>
                            </div>
                            :
                            <div style={{
                                float: "right",
                                marginTop: -7,
                                marginRight: -10,
                                display: "block"
                            }}>
                                <Button onClick={() => {
                                    this.setState({ openDetails: false, selectedRide: null });
                                }}>CLOSE</Button>
                            </div>}
                    </div>
                    <div className="panel-body panelTableBody">
                        {this.state.openDetails === false ? <div className="table-responsive">
                            <table className="col-xs-12 panelTable">
                                <thead>
                                    <tr className="panelTableHead">
                                        <th className="col-md-1">
                                            {" "}
                                            <FormattedMessage id={"rideID"} defaultMessage={"ID"} />
                                        </th>
                                        <th className="col-md-2">
                                            {" "}
                                            <FormattedMessage
                                                id={"riderName"}
                                                defaultMessage={"Name"}
                                            />
                                        </th>
                                        <th className="col-md-2">
                                            <FormattedMessage id={"phoneNo"} defaultMessage={"Contact"} />
                                        </th>
                                        <th className="col-md-2">
                                            {" "}
                                            <FormattedMessage
                                                id={"sourceAddress"}
                                                defaultMessage={"Source"}
                                            />
                                        </th>
                                        <th className="col-md-2">
                                            {" "}
                                            <FormattedMessage
                                                id={"destinationAddress"}
                                                defaultMessage={"Destination"}
                                            />
                                        </th>
                                        <th className="col-md-2">
                                            {" "}
                                            <FormattedMessage
                                                id={"date"}
                                                defaultMessage={"Date"}
                                            />
                                        </th>
                                        <th className="col-md-1">
                                            {" "}
                                            <FormattedMessage
                                                id={"time"}
                                                defaultMessage={"Time"}
                                            />
                                        </th>
                                        {/* <th className="col-md-1" /> */}
                                    </tr>
                                </thead>
                                <tbody className="panelTableTBody">
                                    {
                                        this.state.ridesList !== null ? this.state.ridesList.map((item, index) =>
                                            item._id !== null ? (
                                                <tr key={index}
                                                    onClick={() => {
                                                        this.setState({ openDetails: true, selectedRide: item });
                                                    }}>
                                                    <td>{_.get(item, "_id", "NA")}</td>
                                                    <td>{_.get(item.driverId, "fname", "")} {_.get(item.driverId, "lname", "NA")}</td>
                                                    <td>{_.get(item.riderId, "phoneNo", "NA")}</td>
                                                    <td>{_.get(item, "pickUpAddress", "NA")}</td>
                                                    <td>{_.get(item, "destAddress", "NA")}</td>
                                                    <td>{moment(_.get(item, "bookingTime", "NA")).format("DD MMM YYYY")}</td>
                                                    <td>{moment(_.get(item, "bookingTime", "NA")).format(" h:mm a")}</td>
                                                </tr>) : <h3>No Rides Found</h3>
                                        ) : <h3>No Rides Found</h3>
                                    }
                                </tbody>
                            </table>
                        </div> : <div className="col-xs-12 panelTable"><RideDetails rideObj={this.state.selectedRide} /></div>}
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        ridesHistory: state.users.rideHistory,
    };
}
function bindActions(dispatch) {
    return {
        fetchRideHistory: () =>
            dispatch(UserAction.fetchingRideHistory()),
    };
}

export default connect(mapStateToProps, bindActions)(RideHistory);


