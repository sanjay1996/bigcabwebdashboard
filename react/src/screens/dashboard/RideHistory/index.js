import React, { Component } from "react";
import Layout from "../../../components/Layout/layout";
import RideHistory from "./RideHistory";

export default class RideHistories extends Component {
    render() {
        return (
            <Layout>
                <RideHistory />
            </Layout>
        );
    }
}
