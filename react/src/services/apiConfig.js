export default {
  // local server: "http://13.127.194.178"
  // serverUrl: "http://13.127.194.178", // use Ip address instead of localhost
  serverUrl: "http://192.168.0.116", // use Ip address instead of localhostq
  port: 3020 // incase of running api-server (node app) in development
  // port: 3066 //incase of running api-server (node app) in production
  //  port: 443 //incase of running api-server (node app) on heroku
};
